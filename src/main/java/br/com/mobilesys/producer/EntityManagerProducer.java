package br.com.mobilesys.producer;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Disposes;
import javax.enterprise.inject.Produces;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;

import org.apache.deltaspike.core.api.exclude.Exclude;
import org.apache.deltaspike.core.api.projectstage.ProjectStage;

@ApplicationScoped
@Exclude(ifProjectStage = ProjectStage.UnitTest.class)
public class EntityManagerProducer {

	@PersistenceUnit(unitName = "promoterwebPU", name = "promoterwebPU")
	private EntityManagerFactory emf;

	@Produces
	@RequestScoped
	public EntityManager create() {
		return emf.createEntityManager();
	}

	public void close(@Disposes EntityManager em) {
		if (em.isOpen()) {
			em.close();
		}
	}
}