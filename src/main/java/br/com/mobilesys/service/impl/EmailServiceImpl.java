package br.com.mobilesys.service.impl;

import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;
import java.util.List;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.URLName;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMessage.RecipientType;
import javax.mail.internet.MimeMultipart;

import br.com.mobilesys.service.EmailService;

@Stateless
public class EmailServiceImpl implements EmailService {

	@Resource(name = "java:app/mail/promoterMail")
	private Session mailSession;

	@Override
	public boolean hostEstaNoAr() throws MalformedURLException {

		StringBuilder endereco = new StringBuilder();
		String mailHostmailHost = mailSession.getProperty("mail.host");
		URL url = new URL(endereco.append("http://").append(mailHostmailHost).toString());
		return estaNoAr(url);
	}

	@Override
	public void enviarEmail(List<String> emails, String assunto, String corpoMensagem, String caminhoArquivo) throws MessagingException {
		try {
			File file = null;
			String host = mailSession.getProperty("mail.host");
			String username = mailSession.getProperty("mail.user");
			String password = mailSession.getProperty("mail.password");
			mailSession.setPasswordAuthentication(new URLName("smtp", host, -1, null, username, null),
					new PasswordAuthentication(username, password));

			//criando a mensagem
			MimeMessage message = new MimeMessage(mailSession);
			message.setReplyTo(new javax.mail.Address[] {
					new javax.mail.internet.InternetAddress("contato@mobilesys.com.br")
			});

			//configurando o remetente e o destinatario
			for (String email : emails) {
				message.addRecipient(RecipientType.TO, new InternetAddress(email));

			}

			//configurando a data de envio,  o assunto e o texto da mensagem
			message.setSentDate(new Date());
			message.setSubject(assunto);

			//criando a Multipart
			Multipart multipart = new MimeMultipart();

			//criando a primeira parte da mensagem
			MimeBodyPart attachment0 = new MimeBodyPart();
			//configurando o htmlMessage com o mime type
			attachment0.setContent(corpoMensagem, "text/html; charset=utf-8");
			//adicionando na multipart
			multipart.addBodyPart(attachment0);

			//arquivo que será anexado
//			  caminhoArquivo = "C:/Users/mobilesys.alexandre/Documents/logos/teste2.pdf";
			if (caminhoArquivo != null && !"".equals(caminhoArquivo)) {
				String pathname = caminhoArquivo;//pode conter o caminho
				file = new File(pathname);
			}

			//criando a segunda parte da mensagem
			if (file != null) {
				MimeBodyPart attachment1 = new MimeBodyPart();
				//configurando o DataHandler para o arquivo desejado
				//a leitura dos bytes, descoberta e configuracao do tipo
				//do arquivo serão resolvidos pelo JAF (DataHandler e FileDataSource)

				attachment1.setDataHandler(new DataHandler(new FileDataSource(file)));
				//configurando o nome do arquivo que pode ser diferente do arquivo
				//original Ex: setFileName("outroNome.png")
				attachment1.setFileName(file.getName());
				//adicionando o anexo na multipart
				multipart.addBodyPart(attachment1);
			}

			//adicionando a multipart como conteudo da mensagem 
			message.setContent(multipart);
			//enviando
			Transport.send(message);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private boolean estaNoAr(URL endereco) {
		try {
			HttpURLConnection conn = (HttpURLConnection) endereco.openConnection();
			int codigo = conn.getResponseCode();
			if (codigo == HttpURLConnection.HTTP_OK) {
				return true;
			}
		} catch (IOException e) {

		}
		return false;
	}

	public Session getMailSession() {
		return mailSession;
	}

	public void setMailSession(Session mailSession) {
		this.mailSession = mailSession;
	}

}
