package br.com.mobilesys.service.impl;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;

import br.com.mobilesys.dao.PerguntaDAO;
import br.com.mobilesys.exception.PromoterException;
import br.com.mobilesys.model.Pergunta;
import br.com.mobilesys.model.Pergunta.TipoCampoResposta;
import br.com.mobilesys.repository.PerguntaRepository;
import br.com.mobilesys.service.PerguntaService;
import org.apache.deltaspike.jpa.api.transaction.Transactional;

public class PerguntaServiceImpl implements PerguntaService, Serializable {

	private static final long serialVersionUID = 7286683176145200614L;
	@Inject
	private PerguntaRepository perguntaRepository;
	@Inject
	private PerguntaDAO indexDAO;

	@Override
	public List<Pergunta> listarPerguntas() {
		return perguntaRepository.findAll();
	}

	@Override
	public List<Pergunta> listarPerguntasCheckbox() {
		return perguntaRepository.findOptionalByTipoCampoRespostaEqual(TipoCampoResposta.EMAIL);
	}

	@Override
	public List<Pergunta> listarPerguntasPeloDAO() {
		return indexDAO.listarPerguntas();
	}

	@Override
	public void salvar(Pergunta pergunta) throws PromoterException {
		Pergunta perguntaExistente = obterPorDescricao(pergunta.getDescricao());
		if (perguntaExistente == null) {
			perguntaRepository.save(pergunta);
		} else {
			throw new PromoterException(String.format("Já existe uma pergunta cadastrada com a descrição %s.",pergunta.getDescricao()));
		}
	}

	@Override
	@Transactional
	public void excluir(Pergunta pergunta) {
		perguntaRepository.remove(pergunta);

	}

	@Override
	@Transactional
	public void salvarPeloDAO(Pergunta pergunta) {
		indexDAO.salvar(pergunta);

	}

	@Override
	public Pergunta obterPorId(Long id) {
		return perguntaRepository.findBy(id);
	}

	@Override
	public Pergunta obterPorDescricao(String descricao) {
		return perguntaRepository.obterPorDescricao(descricao);
	}

}
