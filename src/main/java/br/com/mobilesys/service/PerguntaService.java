package br.com.mobilesys.service;

import java.util.List;

import br.com.mobilesys.exception.PromoterException;
import br.com.mobilesys.model.Pergunta;

public interface PerguntaService {

	public List<Pergunta> listarPerguntas();

	public List<Pergunta> listarPerguntasCheckbox();

	public List<Pergunta> listarPerguntasPeloDAO();

	public void salvar(Pergunta pergunta) throws PromoterException;

	public void excluir(Pergunta pergunta);

	public void salvarPeloDAO(Pergunta pergunta);

	public Pergunta obterPorId(Long id);
	
	public Pergunta obterPorDescricao(String descricao);
}
