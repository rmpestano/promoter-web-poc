package br.com.mobilesys.lazy;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import br.com.mobilesys.model.Pergunta;
import br.com.mobilesys.service.PerguntaService;

public class PerguntaLazy extends LazyDataModel<Pergunta> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Inject
	private PerguntaService perguntaService;

	@Override
	public List<Pergunta> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
		List<Pergunta> listarTodos = perguntaService.listarPerguntas();
		int size = listarTodos.size();
		setRowCount(size);
		// paginate
		if (size > pageSize) {
			try {
				return listarTodos.subList(first, first + pageSize);
			} catch (IndexOutOfBoundsException e) {
				return listarTodos.subList(first, first
						+ (size % pageSize));
			}
		}
		return listarTodos;
	}

	@Override
	public Object getRowKey(Pergunta pergunta) {
		return String.valueOf(pergunta.getId());
	}

	@Override
	public Pergunta getRowData(String rowKey) {
		try {
			if (rowKey == null || "null".equals(rowKey)) {
				return null;
			}
			return perguntaService.obterPorId(new Long(rowKey));
		} catch (NumberFormatException e) {
			return null;
		}
	}

}
