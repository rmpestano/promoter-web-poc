package br.com.mobilesys.dao;

import java.io.Serializable;
import java.util.List;

import javax.persistence.EntityManager;

import br.com.mobilesys.model.Pergunta;


//@Stateless
public class PerguntaDAO implements Serializable {

	private static final long serialVersionUID = 1L;
//	@PersistenceContext(name = "promoterweb", type = PersistenceContextType.TRANSACTION)
	private EntityManager entityManager;

	public PerguntaDAO() {
	}
	
	
	public PerguntaDAO(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	public List<Pergunta> listarPerguntas() {
		return null;
//		return entityManager.createQuery("select c FROM Pergunta c", Pergunta.class)
//				.getResultList();
	}


	public void salvar(Pergunta pergunta) {
//		entityManager.persist(pergunta);
	}
	
	public void remover(Pergunta pergunta) {
//		entityManager.remove(pergunta);
	}
	

}
