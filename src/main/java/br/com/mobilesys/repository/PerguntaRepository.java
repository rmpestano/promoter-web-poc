package br.com.mobilesys.repository;

import java.util.List;

import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Query;
import org.apache.deltaspike.data.api.Repository;
import org.apache.deltaspike.data.api.SingleResultType;

import br.com.mobilesys.model.Pergunta;
import br.com.mobilesys.model.Pergunta.TipoCampoResposta;



@Repository
public interface PerguntaRepository extends EntityRepository<Pergunta, Long>{
	
    public List<Pergunta> findOptionalByTipoCampoRespostaEqual(TipoCampoResposta tipoResposta);
    
    @Query(value="select p from Pergunta p where upper(p.descricao) = upper(?1) ",singleResult = SingleResultType.OPTIONAL)
    public Pergunta obterPorDescricao(String descricao);
}

	
