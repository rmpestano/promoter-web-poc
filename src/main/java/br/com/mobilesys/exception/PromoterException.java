package br.com.mobilesys.exception;

import br.com.mobilesys.model.Usuario;

public class PromoterException extends Exception {

	private static final long serialVersionUID = 1L;

	private String idMensagem;
	private Object parametros[];

	/**
	 * Construtor que recebe como parâmetro o id da mensagem (presente no messages.properties) que deverá ser apresentada para o
	 * {@link Usuario} e os parâmetros caso existam.
	 * 
	 * @param idMensagem
	 *            O id da mensagem que será apresentada.
	 * @param parametros
	 *            Os parâmetros que serão utilizados para montar a mensagem.
	 */
	public PromoterException(String idMensagem, Object... parametros) {
		this.idMensagem = idMensagem;
		this.parametros = parametros;
	}

	/**
	 * Construtor que recebe como parâmetro o id da mensagem (presente no messages.properties) que deverá ser apresentada para o
	 * {@link Usuario} e os parâmetros caso existam, também recebe a {@link Exception} mãe.
	 * 
	 * @param throwable
	 *            A {@link Exception} mãe.
	 * @param idMensagem
	 *            O id da mensagem que será apresentada.
	 * @param parametros
	 *            Os parâmetros que serão utilizados para montar a mensagem.
	 */
	public PromoterException(Throwable throwable, String idMensagem, Object... parametros) {
		super(throwable);
		this.idMensagem = idMensagem;
		this.parametros = parametros;
	}

	public PromoterException(Throwable throwable) {
		super(throwable);
	}

	public PromoterException(String message) {
		super(message);
	}

	/**
	 * Obtem o id da mensagem que será exibida.
	 * 
	 * @return O id da mensagem.
	 */
	public String getIdMensagem() {
		return idMensagem;
	}

	/**
	 * Obtem os parametros que serão usados para compor a mensagem.
	 * 
	 * @return Os parametros utilizados na mensagem.
	 */
	public Object[] getParametros() {
		return parametros;
	}

}
