/**
 * 
 */

package br.com.mobilesys.converter;
import javax.el.ValueExpression;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;


/**
 * Converter que será utilizado por todas as {@link Entidade} da aplicação.
 * 
 * @author geraldo - 19/02/2012
 * 
 */
@FacesConverter(value = "enumConverter")
public class EnumConverter implements Converter {

	/**
	 * Construtor da classe.
	 */
	public EnumConverter() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.faces.convert.Converter#getAsObject(javax.faces.context.FacesContext, javax.faces.component.UIComponent, java.lang.String)
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		try {
			if (value != null && !value.equals("null") && !value.isEmpty()) {
				ValueExpression valueExpression = component.getValueExpression("value");
				if (valueExpression == null) {
					return null;
				}
				Class classe = valueExpression.getType(context.getELContext());
				if (classe == null) {
					return null;
				}
				return Enum.valueOf(classe, value);
			}
		} catch (Exception e) {
			return null;
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.faces.convert.Converter#getAsString(javax.faces.context.FacesContext, javax.faces.component.UIComponent, java.lang.Object)
	 */
	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value == null) {
			return null;
		}
		return ((Enum<?>) value).name();
	}

}
