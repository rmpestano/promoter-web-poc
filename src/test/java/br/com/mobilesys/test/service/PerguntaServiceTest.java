package br.com.mobilesys.test.service;

import java.util.List;

import javax.inject.Inject;

import org.apache.deltaspike.core.api.projectstage.ProjectStage;
import org.apache.deltaspike.testcontrol.api.TestControl;
import org.apache.deltaspike.testcontrol.api.junit.CdiTestRunner;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import br.com.mobilesys.model.Pergunta;
import br.com.mobilesys.service.PerguntaService;

import com.github.dbunit.rules.cdi.api.UsingDataSet;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(CdiTestRunner.class)
@TestControl(projectStage = ProjectStage.UnitTest.class)
public class PerguntaServiceTest {

	@Inject
	private PerguntaService perguntaService;

	@Test
	@UsingDataSet("pergunta.yml")
	public void deveRetornarLista() {
		List<Pergunta> listarPerguntas = perguntaService.listarPerguntas();
		Assert.assertEquals(2, listarPerguntas.size());
	}

	@Test
	@UsingDataSet("pergunta.yml")
	public void deveExcluirPergunta() {
		Pergunta pergunta= perguntaService.obterPorDescricao("pergunta1");
		assertThat(pergunta).isNotNull();
		assertThat(pergunta.getDescricao()).isEqualTo("pergunta1");
		perguntaService.excluir(pergunta);
		pergunta= perguntaService.obterPorDescricao("pergunta1");
		assertThat(pergunta).isNull();
	}

}
