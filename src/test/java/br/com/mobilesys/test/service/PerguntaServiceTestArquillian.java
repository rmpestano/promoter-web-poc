package br.com.mobilesys.test.service;

import static org.junit.Assert.assertNotNull;

import java.util.List;

import javax.inject.Inject;

import org.junit.Test;

import br.com.mobilesys.model.Pergunta;
import br.com.mobilesys.service.PerguntaService;

//@RunWith(Arquillian.class)
public class PerguntaServiceTestArquillian {

//	@Deployment
//	public static WebArchive createDeployment() {
//		WebArchive war = ShrinkWrap
//				.create(WebArchive.class, "teste-pergunta-unit-test.war")
//				.addClasses(PerguntaService.class, PerguntaServiceImpl.class, Pergunta.class, PromoterException.class,
//						PerguntaRepository.class)
//				.addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");
//		System.out.println(war.toString(true));
//		return war;
//	}

//	@Inject
	private PerguntaService perguntaService;

//	@Test
	public void deveTrazerListaPerguntas() {
		List<Pergunta> listarPerguntas = perguntaService.listarPerguntas();
		assertNotNull(listarPerguntas);
	}

}
