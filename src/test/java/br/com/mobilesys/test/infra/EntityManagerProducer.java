
package br.com.mobilesys.test.infra;
import com.github.dbunit.rules.cdi.util.EntityManagerProvider;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;
import javax.persistence.EntityManager;

import org.apache.deltaspike.core.api.exclude.Exclude;
import org.apache.deltaspike.core.api.projectstage.ProjectStage;

/**
 * Created by pestano on 11/02/16.
 */
@ApplicationScoped
@Exclude(ifProjectStage = ProjectStage.Development.class)
public class EntityManagerProducer {


    @Produces
    public EntityManager produce() {
        return EntityManagerProvider.instance("testpu").em();
    }

}
